This repo deploys AWS infra using CDK, including: lambdas, sqs, es, dynamo, and security groups.

CDK is in `./infrastructure`
Lambdas are in `./src`

It was made to listen to magic eden transactions as a sort of rarity sniper-bot.

Lambdas are triggered on a regular timer with cloudwatch, which may not be ideal.
