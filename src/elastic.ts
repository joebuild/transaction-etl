import { HttpRequest} from "@aws-sdk/protocol-http";
import { defaultProvider } from  "@aws-sdk/credential-provider-node";
import { SignatureV4 }  from  "@aws-sdk/signature-v4";
import { NodeHttpHandler } from "@aws-sdk/node-http-handler";
import { Sha256 } from "@aws-crypto/sha256-browser";
import { AllMetadata } from './metaplex/metadata'

export const ES_ENDPOINT = 'REDACTED.us-east-1.es.amazonaws.com'

export const TRANSACTIONS_INDEX = 'transactions'
export const METADATA_INDEX = 'metadata'
export const TRAIT_INDEX = 'traits'

// Sign the request
const signer = new SignatureV4({
  credentials: defaultProvider(),
  region: 'us-east-1',
  service: 'es',
  sha256: Sha256
});

const client = new NodeHttpHandler();

export async function indexDocument(index: string, document: any, id: string): Promise<any> {
  const request: HttpRequest = getHttpRequest(document, 'PUT', index, '/_doc/' + id)
  const signedRequest = await signer.sign(request);

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('Response body: ' + responseBody);
      resolve(responseBody)
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function getCollection(updateAuthority: string): Promise<any> {
  const query = {
    "from" : 0, "size" : 10000,
    "query": {
      "match": {
        "updateAuthority": updateAuthority
      }
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_search')
  const signedRequest = await signer.sign(request);

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('search finished');
      resolve(JSON.parse(responseBody))
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function getRaritySortedCollection(updateAuthority: string): Promise<any> {
  const query = {
    "from" : 0, "size" : 10000,
    "sort" : [
      { "rarity" : "asc" },
    ],
    "query": {
      "match": {
        "updateAuthority": updateAuthority
      }
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_search')
  const signedRequest = await signer.sign(request);

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('search finished');
      resolve(JSON.parse(responseBody))
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function calculateTraitCount(updateAuthority: string, trait_type: string, value: string): Promise<any> {
  const query = {
    "query":{
      "bool":{
        "must":[
          {
            "nested": {
              "path": "metadataJson.attributes",
              "query": {
                "bool": {
                  "filter": [
                    {
                      "term": {
                        "metadataJson.attributes.trait_type": trait_type
                      }
                    },
                    {
                      "term": {
                        "metadataJson.attributes.value": value
                      }
                    }
                  ]
                }
              }
            }
          },
          {
            "match": {
              "updateAuthority": updateAuthority
            }
          }
        ]
      }
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_count')
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('search finished');
      resolve(JSON.parse(responseBody))
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function getTraitCount(id: string): Promise<any> {
  const request: HttpRequest = getHttpRequestNoBody('GET', TRAIT_INDEX, '/_doc/' + id)
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('search finished');
      resolve(JSON.parse(responseBody))
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function getNftMetadata(mint: string): Promise<any> {
  const request: HttpRequest = getHttpRequestNoBody('GET', METADATA_INDEX, '/_doc/' + mint)
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('search finished');
      resolve(JSON.parse(responseBody))
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function setRarity(mint: string, rarity: number): Promise<any> {
  const query = {
    "doc": {
      "rarity": rarity
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_doc/' + mint + '/_update')
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('Response body: ' + responseBody);
      resolve(responseBody)
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function setCollectionSize(mint: string, collectionSize: number): Promise<any> {
  const query = {
    "doc": {
      "collectionSize": collectionSize
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_doc/' + mint + '/_update')
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('Response body: ' + responseBody);
      resolve(responseBody)
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export async function setRank(mint: string, rank: number): Promise<any> {
  const query = {
    "doc": {
      "rank": rank
    }
  }

  const request: HttpRequest = getHttpRequest(query, 'POST', METADATA_INDEX, '/_doc/' + mint + '/_update')
  const signedRequest = await signer.sign(request);

  // Send the request

  const { response } =  await client.handle(<HttpRequest>signedRequest)
  console.log(response.statusCode + ' ' + response.body.statusMessage);

  let responseBody = '';
  return new Promise((resolve) => {
    response.body.on('data', (chunk: any) => {
      responseBody += chunk;
    });
    response.body.on('end', () => {
      console.log('Response body: ' + responseBody);
      resolve(responseBody)
    });
  }).catch((error) => {
    console.log('Error: ' + error);
  });
}

export const getHttpRequest = (query: any, method: string, index: string, path: string): HttpRequest => {
  return new HttpRequest({
    body: JSON.stringify(query),
    headers: {
      'Content-Type': 'application/json',
      'host': ES_ENDPOINT
    },
    hostname: ES_ENDPOINT,
    method: method,
    path: index + path
  });
}

export const getHttpRequestNoBody = (method: string, index: string, path: string): HttpRequest => {
  return new HttpRequest({
    headers: {
      'Content-Type': 'application/json',
      'host': ES_ENDPOINT
    },
    hostname: ES_ENDPOINT,
    method: method,
    path: index + path
  });
}

export const getTraitId = (updateAuthority: string, traitType: string, value: string) => {
  const regex = /[^A-Za-z0-9]/g;

  let traitType_ = traitType
  if (traitType.length > 1){
    traitType_ = traitType_.toLowerCase().replace(regex, "")
  }

  let traitValue_ = value
  if (value.length > 1){
    traitValue_ = traitValue_.toLowerCase().replace(regex, "")
  }

  return updateAuthority + '-' + traitType_ + '-' + traitValue_
}
