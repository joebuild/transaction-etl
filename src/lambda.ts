import { Context, ScheduledEvent, SQSEvent } from 'aws-lambda'
import { Connection, LAMPORTS_PER_SOL, PublicKey, TransactionResponse } from '@solana/web3.js'
import { GetQueueUrlCommand, SendMessageResult } from '@aws-sdk/client-sqs'
import { sqsClient } from './sqsClient'
import {
  calculateTraitCount,
  getCollection,
  getNftMetadata,
  getRaritySortedCollection,
  getTraitCount,
  getTraitId,
  indexDocument,
  setCollectionSize,
  setRank,
  setRarity,
  TRAIT_INDEX,
  TRANSACTIONS_INDEX,
} from './elastic'
import { AWSError } from 'aws-sdk'
import {
  dynamoGetCollectionRecord,
  dynamoGetKey,
  dynamoPutKey,
  dynamoStartCollectionCount, MINT_TXS_TABLE,
  MINTS_TABLE,
  TRANSACTIONS_TABLE,
} from './dynamo'
import {
  addMetadataAndJsonToEs,
  AllMetadata,
  getAllMetadata,
  getAllMetadataByUpdateAuthority,
} from './metaplex/metadata'
import { getMintTransactions } from './metaplex/candyMachine'
import { Metadata } from '@metaplex-foundation/mpl-token-metadata'

let async = require("async");

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const MAGIC_EDEN = new PublicKey('M2mx93ekt1fmXSVkTrUL9xVFHkmME8HTUi5Cyc5aF7K');

/*
This lambda creates a websocket connection to monitor updates to accounts which are owned by the Magic Eden program.
It then adds those account addresses to the 'state' queue.
 */
export async function websocketHandler(event: ScheduledEvent) {
  const queue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "states" }));

  while (true){
    try {
      await (async () => {
        const connection = new Connection('https://ssc-dao.genesysgo.net/', 'confirmed' );

        connection.onProgramAccountChange(
          MAGIC_EDEN,
          async (updatedAccountInfo, context) => {
            try {
              const body = JSON.stringify(updatedAccountInfo)
              await sqs.sendMessage(
                {
                  MessageBody: body,
                  QueueUrl: queue.QueueUrl
                },
                function(err: AWSError, data: SendMessageResult) {
                  if (err) {
                    console.log('SQS Error:', err)
                    return
                  }
                }
              ).promise();
            } catch (e) {
              console.log(e)
            }
          },
          'finalized',
        );
      })();

      await delay(5 * 60 - 5);
    } catch (e){
      console.log(e)
    }
  }
}

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

/*
This lambda consumes from the 'state' queue, which holds account state changes.
It saves the PDA state to the database.
It then pulls transactions for those accounts and puts them on the 'transaction' queue.
 */
export async function stateHandler(event: SQSEvent, context: Context) {
  const connection = new Connection('https://ssc-dao.genesysgo.net/', 'confirmed' );

  const transactionQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "transactions" }));
  const mintQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "mints" }));

  return await Promise.all(event.Records.map(async (record) => {
    const updatedAccountInfo = JSON.parse(record.body)
    try {
      await getTxs(new PublicKey(updatedAccountInfo.accountId), connection, transactionQueue.QueueUrl || '', mintQueue.QueueUrl || '')
    } catch (e){
      console.log(e)
    }
  }))
}

const getTxs = async (account: PublicKey, connection: Connection, transactionQueueUrl: string, mintQueueUrl: string) => {
  const sigs = (await connection.getSignaturesForAddress(account, {limit: 1}, 'confirmed'))
    .filter((x) => !x.err)

  let txs: (TransactionResponse | null)[] = await Promise.all(
    sigs.map(async (sig) => {
      const record = await dynamoGetKey(sig.signature, TRANSACTIONS_TABLE)

      if (record?.Item === undefined){
        const tx = connection.getTransaction(sig.signature, {commitment: 'confirmed'})
        await dynamoPutKey(sig.signature, TRANSACTIONS_TABLE)
        return tx
      } else {
        return null
      }
    })
  )

  const uniqueTxs = txs.filter((value, index) => {
    const _value = JSON.stringify(value);
    return index === txs.findIndex(obj => {
      return JSON.stringify(obj) === _value;
    });
  });

  return await Promise.all(
    uniqueTxs
      .filter((x) => x !== null)
      .map(async (x) => {
        await queueMintFromTx(x as TransactionResponse, mintQueueUrl)

        return await sqs.sendMessage(
          {
            MessageBody: JSON.stringify(x),
            QueueUrl: transactionQueueUrl
          },
          function(err: AWSError, data: SendMessageResult) {
            if (err) {
              console.log('SQS Error:', err)
              return
            }
          }
        ).promise();
      })
  );
}

const queueMintFromTx = async (txRsp: TransactionResponse, queueUrl: string) => {
  if (txRsp.meta?.postTokenBalances){
    let mints = txRsp.meta.postTokenBalances.map((x) => x.mint)
    mints = [...new Set(mints)];

    for (const mint of mints){
      const record = await dynamoGetKey(mint, MINTS_TABLE)
      if (record?.Item === undefined) {
        const body = JSON.stringify(mint)
        await sqs.sendMessage(
          {
            MessageBody: body,
            QueueUrl: queueUrl
          },
          function(err: AWSError, data: SendMessageResult) {
            if (err) {
              console.log('SQS Error:', err)
              return
            }
          }
        ).promise();
      }
    }
  } else {
    return null
  }
}

/*
This lambda consumes from the 'transaction' queue.
It saves the transactions to the database.
 */
export async function transactionHandler(event: SQSEvent) {

  const txs = event.Records.map((record) => JSON.parse(record.body))

  const uniqueTxs = txs.filter((value, index) => {
    const _value = JSON.stringify(value);
    return index === txs.findIndex(obj => {
      return JSON.stringify(obj) === _value;
    });
  });

  await Promise.all(
    uniqueTxs.map(async (tx) => await checkTxForDeal(tx))
  )

  return await Promise.all(
    event.Records.map( async (record) => {
      const recordJson = JSON.parse(record.body);
      return await indexDocument(TRANSACTIONS_INDEX, recordJson, recordJson?.transaction?.signatures[0])
    })
  )
}

/*
This lambda pulls the transactions from the candy machine
 */
export async function metadataTxHandler(event: SQSEvent) {
  const connection = new Connection('https://ssc-dao.genesysgo.net/', 'confirmed' );
  const metadataTxQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "metadata_tx" }));

  await async.mapLimit(
    event.Records,
    3,
    async (record: any) => {
      const mint = new PublicKey(JSON.parse(record.body))
      const key = await dynamoGetKey(JSON.parse(record.body), MINTS_TABLE)
      if (key?.Item === undefined) {
        try {
          const mintTxs = await getMintTransactions(connection, mint)

          for (const mintTx of mintTxs) {
            const record = await dynamoGetKey(mintTx, MINT_TXS_TABLE)
            if (record?.Item === undefined) {
              await dynamoPutKey(mintTx, MINT_TXS_TABLE)

              const body = JSON.stringify(mintTx)
              await sqs.sendMessage(
                {
                  MessageBody: body,
                  QueueUrl: metadataTxQueue.QueueUrl
                },
                function(err: AWSError, data: SendMessageResult) {
                  if (err) {
                    console.log('SQS Error:', err)
                    return
                  }
                }
              ).promise();
            }
          }
        } catch (e) {
          console.log(e)
        }
      }
    }
  )

}

export async function metadataHandler(event: SQSEvent) {
  const connection = new Connection('https://ssc-dao.genesysgo.net/', 'confirmed' );
  const metadataQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "metadata" }));

  const mint = new PublicKey(JSON.parse(event.Records[0].body))

  const sampleMetadata = await getAllMetadata(connection, mint)

  const collectionMetadata = await getAllMetadataByUpdateAuthority(connection, new PublicKey(sampleMetadata.updateAuthority))

  const collectionRecord = await dynamoGetCollectionRecord(sampleMetadata.updateAuthority, sampleMetadata.metadataJson?.symbol || '')
  if (collectionRecord?.Item === undefined) {
    await dynamoStartCollectionCount(sampleMetadata.updateAuthority, sampleMetadata.metadataJson?.symbol || '', collectionMetadata.length)
  }

  await async.mapLimit(
    collectionMetadata,
    5,
    async (metadata: AllMetadata) => {
      const record = await dynamoGetKey(metadata.mint, MINTS_TABLE)
      if (record?.Item === undefined) {
        await dynamoPutKey(metadata.mint, MINTS_TABLE)

        try {
          const body = JSON.stringify(metadata)
          await sqs.sendMessage(
            {
              MessageBody: body,
              QueueUrl: metadataQueue.QueueUrl
            },
            function(err: AWSError, data: SendMessageResult) {
              if (err) {
                console.log('SQS Error:', err)
                return
              }
            }
          ).promise();
        } catch (e) {
          console.log(e)
        }
      }
    }
  )
}

/*
This lambda pulls the metadata JSON for the collection and adds everything to ES
 */
export async function metadataJsonHandler(event: SQSEvent) {
  const traitsQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "traits" }));

  for (const record of event.Records){
    let allMetadata = JSON.parse(record.body)
    await addMetadataAndJsonToEs(allMetadata, sqs, traitsQueue.QueueUrl || '')
  }
}

/*
This lambda stores trait frequency for collections in the ES traits table
 */
export async function traitHandler(event: SQSEvent) {
  const allMetadataSample = JSON.parse(event.Records[0].body)

  const collectionRecord = await dynamoGetCollectionRecord(allMetadataSample.updateAuthority, allMetadataSample.metadataData.data.symbol)

  if (collectionRecord.Item?.numProcessed >= collectionRecord.Item?.size){
    const queryResponse = await getCollection(allMetadataSample.updateAuthority)

    const collectionSize: number = queryResponse?.hits?.total?.value
    console.log('collection size:', collectionSize)

    let traits = {}

    for (const hit of queryResponse?.hits?.hits) {
      const attributes = hit['_source']?.metadataJson?.attributes

      for (const attribute of attributes) {
        // @ts-ignore
        if (!traits[attribute.trait_type]) {
          // @ts-ignore
          traits[attribute.trait_type] = new Set()
        }

        // @ts-ignore
        traits[attribute.trait_type] = (traits[attribute.trait_type] as Set).add(attribute.value)
      }
    }

    await async.mapLimit(
      [...Object.keys(traits)],
      3,
      async (traitType: any) => {
        // @ts-ignore
        for (let value of traits[traitType]) {
          const traitCountRecord = await calculateTraitCount(allMetadataSample.updateAuthority, traitType, value)

          const doc = {
            updateAuthority: allMetadataSample.updateAuthority,
            trait_type: traitType,
            value: value,
            count: traitCountRecord.count,
            collectionCount: collectionSize,
          }

          try {
            const id = getTraitId(allMetadataSample.updateAuthority, traitType, value)
            await indexDocument(TRAIT_INDEX, doc, id)
          } catch (e){
            console.log(e)
          }
        }
      }
    )

    const rarityQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "rarity" }));
    const body = JSON.stringify(allMetadataSample)
    await sqs.sendMessage(
      {
        MessageBody: body,
        QueueUrl: rarityQueue.QueueUrl
      },
      function(err: AWSError, data: SendMessageResult) {
        if (err) {
          console.log('SQS Error:', err)
          return
        }
      }
    ).promise();

    console.log(traits)
  }
}

/*
This lambda calculates rarity for all mints in a collection, using the trait frequency
 */
export async function rarityHandler(event: SQSEvent) {
  const allMetadataSample = JSON.parse(event.Records[0].body)

  console.log('getting collection...')
  const queryResponse = await getCollection(allMetadataSample.updateAuthority)
  console.log('got collection.')

  await async.mapLimit(
    queryResponse?.hits?.hits,
    3,
    async (hit: any) => {
      const nft = hit['_source']
      const attributes = nft?.metadataJson?.attributes

      const rarities = await async.mapLimit(
        [...attributes],
        3,
        async (attribute: any) => {
          const traitType = attribute.trait_type
          const value = attribute.value

          if (traitType !== null && value !== null && traitType !== undefined && value !== undefined){
            const id = getTraitId(allMetadataSample.updateAuthority, traitType, value)

            const traitRecord = await getTraitCount(id)

            const traitCount = traitRecord['_source']?.count || 0
            const collectionCount = traitRecord['_source']?.collectionCount || 1.0

            if (traitCount > 0 && collectionCount > 100){
              return (traitCount/collectionCount)
            } else {
              return 1
            }
          } else {
            return 1
          }
        }
      )

      const rarity =  rarities.reduce((total: number, item: number) => total * item, 1.0);

      console.log('rarity', rarity)

      await setRarity(nft?.mint, rarity)
      await setCollectionSize(nft?.mint, queryResponse?.hits?.total?.value)
    }
  )

  const rankQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "rank" }));
  const body = JSON.stringify(allMetadataSample)
  await sqs.sendMessage(
    {
      MessageBody: body,
      QueueUrl: rankQueue.QueueUrl
    },
    function(err: AWSError, data: SendMessageResult) {
      if (err) {
        console.log('SQS Error:', err)
        return
      }
    }
  ).promise();

  console.log('finished rarity for collection of size:', queryResponse?.hits?.total?.value)
}

/*
This lambda calculates rarity for all mints in a collection, using the trait frequency
 */
export async function rankHandler(event: SQSEvent) {
  const allMetadataSample: AllMetadata = JSON.parse(event.Records[0].body)

  console.log('getting collection sorted by rarity...')
  const queryResponse = await getRaritySortedCollection(allMetadataSample.updateAuthority)

  for (const [i, hit] of queryResponse?.hits?.hits.entries()) {
    const rank = i + 1
    await setRank(hit['_source']?.mint, rank)
  }

  console.log('finished rankings for collection of size:', queryResponse?.hits?.total?.value)
}


export async function checkTxForDeal(tx: any){
  try {
    const messages = tx?.meta?.logMessages as string[]

    const sellMessage = "seller_expiry"

    console.log('checking tx for deal')

    if (messages.map((x) => x.includes(sellMessage)).includes(true)){
      console.log('tx included sell message')

      const mints = tx.meta.postTokenBalances.map((x: any) => x.mint)

      const nft = (await getNftMetadata(mints[0]))['_source'] as AllMetadata

      let priceSol = 0

      try {
        priceSol = parseInt(messages.filter((x: string) => x.includes('price'))[0].split(',')[0].split(':')[2]) / LAMPORTS_PER_SOL
      } catch (e){
        console.log(e)
        console.log(messages)
      }

      if (nft?.collectionSize && nft?.rank && (nft.rank < 0.05 * nft.collectionSize) && priceSol < 100){
        console.log('item is rare')

        const discordQueue = await sqsClient.send(new GetQueueUrlCommand({ QueueName: "discord" }));
        const body = JSON.stringify({ ...nft, priceLamports: priceSol })
        await sqs.sendMessage(
          {
            MessageBody: body,
            QueueUrl: discordQueue.QueueUrl
          },
          function(err: AWSError, data: SendMessageResult) {
            if (err) {
              console.log('SQS Error:', err)
              return
            }
          }
        ).promise();
      }
    }
  } catch (e) {
    console.log(e)
  }
}
