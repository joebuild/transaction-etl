import { Connection, GetProgramAccountsFilter, PublicKey } from '@solana/web3.js'
import { AllMetadata, getAllMetadata, getAllMetadataFromMintList } from './metadata'
let async = require("async");

const CANDY_MACHINE_PROGRAM_V1_ID = new PublicKey('cndyAnrLdpjq1Ssp1z8xxDsB8dxe7u4HL5Nxi2K5WXZ');
const CANDY_MACHINE_PROGRAM_V2_ID = new PublicKey('cndy3Z4yapfJBmL3ShUp5exZKqR3z33thTzeNMm2gRZ');

export async function getMintTransactions(connection: Connection, seedMint: PublicKey): Promise<string[]> {
  const txs = await connection.getSignaturesForAddress(seedMint)

  if (txs.length > 0) {
    const mintSig = txs.slice(-1)[0]

    const mintTx = await connection.getTransaction(mintSig.signature)

    const candyMachineId = mintTx?.transaction.message.accountKeys[3]

    let allMintTxs = new Set<string>();

    for (const x of [0, 1, 2]) {
      let mintTxs = new Set<string>();
      let size = 0
      let hasMore = true

      let beforeTx = null

      while (hasMore) {
        // @ts-ignore
        const newTxs = await connection.getSignaturesForAddress(candyMachineId as PublicKey, beforeTx ? { before: beforeTx } : {})

        newTxs.forEach((x: any) => {
          mintTxs.add(x.signature)
        })

        beforeTx = newTxs.length ? newTxs.slice(-1)[0].signature : beforeTx

        if (mintTxs.size <= size) {
          hasMore = false
        }
        size = mintTxs.size
      }

      mintTxs.forEach((x) => allMintTxs.add(x))
    }

    console.log(allMintTxs.size, 'transactions')

    return [...allMintTxs]
  } else {
    return []
  }
}

export async function getMetadataFromMintTransaction(connection: Connection, mintTx: string): Promise<AllMetadata[]> {
  let mints = new Set<string>();

  const tx = await connection.getTransaction(mintTx)

  if (tx?.meta && tx.meta?.preTokenBalances?.length){
    for (const ptBal of tx.meta?.preTokenBalances){
      mints.add(ptBal.mint)
    }
  }

  if (tx?.meta && tx.meta?.postTokenBalances?.length){
    for (const ptBal of tx.meta?.postTokenBalances){
      mints.add(ptBal.mint)
    }
  }

  return Promise.all(
    [...mints].map(async (mint) => {
      return await getAllMetadata(connection, new PublicKey(mint))
    })
  )
}

export const authorityFilter = (
  authority: PublicKey,
): GetProgramAccountsFilter => ({
  memcmp: {
    offset: 8, // discriminator
    bytes: authority.toBase58(),
  },
});
