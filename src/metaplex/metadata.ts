import { Connection, PublicKey } from '@solana/web3.js'
import { Metadata, MetadataData } from '@metaplex-foundation/mpl-token-metadata'
import { MetadataJson } from '@metaplex/js/src/types'
import axios, { AxiosResponse } from 'axios'
import { dynamoGetCollectionRecord, dynamoIncrementCollectionNumProcessedCount } from '../dynamo'
import { indexDocument, METADATA_INDEX } from '../elastic'
import { AWSError, SQS } from 'aws-sdk'
import { SendMessageResult } from '@aws-sdk/client-sqs'

let async = require("async");


export type AllMetadata = {
  metadataData: MetadataData,   // on-chain
  metadataJson?: MetadataJson,  // off-chain JSON file w/ attributes
  mint: string,              // for convenience and indexing in ES
  updateAuthority: string,   // for convenience and indexing in ES
  rarity: number,
  rank?: number,
  collectionSize?: number,
}

export async function addMetadataAndJsonToEs(allMetadataTemp: AllMetadata, sqs: SQS, queueUrl: string){
  try {
    let allMetadata = allMetadataTemp

    await dynamoIncrementCollectionNumProcessedCount(allMetadata.updateAuthority, allMetadata.metadataJson?.symbol || '')

    allMetadata.metadataJson = await lookup(allMetadata.metadataData.data.uri);

    await indexDocument(METADATA_INDEX, allMetadata, allMetadata.mint)

    const collectionRecord = await dynamoGetCollectionRecord(allMetadataTemp.updateAuthority, allMetadataTemp.metadataData.data.symbol)

    if (collectionRecord.Item?.numProcessed >= 100){
      await sqs.sendMessage(
        {
          MessageBody: JSON.stringify(allMetadata),
          QueueUrl: queueUrl
        },
        function(err: AWSError, data: SendMessageResult) {
          if (err) {
            console.log('SQS Error:', err)
            return
          }
        }
      ).promise();
    }

    return allMetadata
  } catch (e) {
    console.log('Error getting metadataJson:', e)
    return null
  }
}

export const getAllMetadata = async (connection: Connection, mint: PublicKey): Promise<AllMetadata> => {
  const metadata = await Metadata.findByMint(connection, mint);
  const metadataJson = await lookup(metadata.data.data.uri);

  const updateAuthority = metadata.data.updateAuthority

  return {
    metadataData: metadata.data,
    metadataJson,
    mint: mint.toBase58(),
    rarity: 1.0,
    updateAuthority
  }
};

export async function getAllMetadataByUpdateAuthority(connection: Connection, authority: PublicKey): Promise<AllMetadata[]> {
  const metadata = await Metadata.findMany(connection, {updateAuthority: authority})

  return metadata.map((metadata) => {
      return {
        metadataData: metadata.data,
        mint: metadata.data.mint,
        rarity: 1.0,
        updateAuthority: metadata.data.updateAuthority
      }
    });
}

export async function getAllMetadataFromMintList(connection: Connection, mints: string[]): Promise<AllMetadata[]> {
  const metadata: Metadata[] = await async.mapLimit(
    [...mints],
    3,
    async (mint: any) => {
      try {
        return await Metadata.findByMint(connection, mint);
      } catch (e){
        return null
      }
    }
  )

  return metadata
    .filter((x) => x !== null)
    .map((x) => {
      return {
        metadataData: x.data,
        mint: x.data.mint,
        rarity: 1.0,
        updateAuthority: x.data.updateAuthority
      }
    });
}

const lookup = async (url: string): Promise<MetadataJson> => {
  try {
    const { data } = await axios.get<string, AxiosResponse<MetadataJson>>(url);
    return data;
  } catch {
    throw new Error(`unable to get metadata json from url ${url}`);
  }
};
