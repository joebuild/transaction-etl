import { DynamoDB } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument, PutCommandOutput } from '@aws-sdk/lib-dynamodb'
import { GetCommandOutput } from '@aws-sdk/lib-dynamodb/dist-types/commands/GetCommand'

const client = new DynamoDB({});
const docClient = DynamoDBDocument.from(client);

export const TRANSACTIONS_TABLE = 'Transactions'
export const MINTS_TABLE = 'Mints'
export const MINT_TXS_TABLE = 'MintTxs'
export const COLLECTIONS_TABLE = 'Collections'

export const dynamoPutKey = async (value: string, table: string): Promise<void> => {
  try {
    return docClient.put({
      TableName: table,
      Item: {
        id: value,
      },
    },
  function(err, data) {
        if (err) console.log(err);
        else console.log(data);
        return data
      }
    );
  } catch (e) {
    console.log(e)
  }
}

export const dynamoGetKey = async (value: string, table: string): Promise<(GetCommandOutput | null)> => {
  try {
    return docClient.get({
      TableName: table,
      Key: {
        id: value,
        // sortKey: "VALUE",
      },
    });
  } catch (e) {
    return null
  }
}

export const dynamoStartCollectionCount = async (updateAuthority: string, collectionSymbol: string, size: number): Promise<void> => {
  const key = getCollectionKey(updateAuthority, collectionSymbol)
  try {
    return docClient.put({
        TableName: COLLECTIONS_TABLE,
        Item: {
          id: key,
          size: size,
          numProcessed: 0,
        },
      },
      function(err, data) {
        if (err) console.log(err);
        else console.log(data);
        return data
      }
    );
  } catch (e) {
    console.log(e)
  }
}

export const dynamoGetCollectionRecord = async (updateAuthority: string, collectionSymbol: string): Promise<GetCommandOutput> => {
  const key = getCollectionKey(updateAuthority, collectionSymbol)
  return docClient.get(
    {
      TableName: COLLECTIONS_TABLE,
      Key: {
        id: key,
        // sortKey: "VALUE",
      },
    }
  );
}

export const dynamoIncrementCollectionNumProcessedCount = async (updateAuthority: string, collectionSymbol: string): Promise<void> => {
  const key = getCollectionKey(updateAuthority, collectionSymbol)
  try {
    return docClient.update({
        TableName: COLLECTIONS_TABLE,
        Key: {
          id: key
        },
        UpdateExpression: "SET numProcessed = if_not_exists(numProcessed, :start) + :inc",
        ExpressionAttributeValues: {
          ':inc': 1,
          ':start': 0,
        },
        ReturnValues: "UPDATED_NEW"
      },
      function(err, data) {
        if (err) console.log(err);
        else console.log(data);
        return data
      }
    );
  } catch (e) {
    console.log(e)
  }
}

const getCollectionKey = (updateAuthority: string, collectionSymbol: string) => {
  const regex = /[^A-Za-z0-9]/g;
  return updateAuthority + '-' + collectionSymbol.replace(regex, "")
}
