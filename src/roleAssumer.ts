import { STS, AssumeRoleRequest } from '@aws-sdk/client-sts'
import { Credentials } from '@aws-sdk/types'

export const roleAssumer = async (
  sourceCreds: Credentials,
  assumeRoleRequest: AssumeRoleRequest
) => {
  const sts = new STS({ region: 'us-east-1', credentials: sourceCreds })
  const { Credentials } = await sts.assumeRole(assumeRoleRequest)
  if (!Credentials) throw Error('Could not assume role')
  return {
    accessKeyId: Credentials.AccessKeyId!,
    secretAccessKey: Credentials.SecretAccessKey!,
    sessionToken: Credentials.SessionToken!,
    expiration: Credentials.Expiration!,
  }
}
