import { Construct, Duration, Stack, StackProps } from '@aws-cdk/core'
import { Code, Function, Runtime } from '@aws-cdk/aws-lambda'
import { RetentionDays } from '@aws-cdk/aws-logs'
import { Rule, Schedule } from '@aws-cdk/aws-events'
import { LambdaFunction } from '@aws-cdk/aws-events-targets'
import * as sqs from '@aws-cdk/aws-sqs'
import { SqsEventSource } from '@aws-cdk/aws-lambda-event-sources'
import * as iam from '@aws-cdk/aws-iam'
import { Effect } from '@aws-cdk/aws-iam'
import * as dynamodb from '@aws-cdk/aws-dynamodb';

export class LambdaStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props)

    const websocketLambda = new Function(this, 'Websocket', {
      functionName: 'Websocket',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.websocketHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 1024,
      timeout: Duration.seconds(5 * 60 - 1),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 1
    })

    new Rule(this, 'WebsocketSchedule', {
      description: 'Triggers my lambda function on some schedule',
      schedule: Schedule.rate(Duration.minutes(5)),
      targets: [new LambdaFunction(websocketLambda)],
    })

    const stateLambda = new Function(this, 'State', {
      functionName: 'State',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.stateHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 512,
      timeout: Duration.minutes(3),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 50
    })

    const stateQueue = new sqs.Queue(this, 'states', {visibilityTimeout: Duration.minutes(15), queueName: 'states' });
    const stateEventSource = new SqsEventSource(stateQueue);
    stateLambda.addEventSource(stateEventSource);

    const transactionLambda = new Function(this, 'Transaction', {
      functionName: 'Transaction',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.transactionHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 512,
      timeout: Duration.minutes(3),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 1
    })

    const transactionQueue = new sqs.Queue(this, 'transactions', {visibilityTimeout: Duration.minutes(3), queueName: 'transactions' });
    const transactionEventSource = new SqsEventSource(transactionQueue);
    transactionLambda.addEventSource(transactionEventSource);

    const metadataLambda = new Function(this, 'Metadata', {
      functionName: 'Metadata',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.metadataHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 2048,
      timeout: Duration.minutes(15),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 10
    })

    const mintQueue = new sqs.Queue(this, 'mints', {visibilityTimeout: Duration.minutes(15), queueName: 'mints' });
    const mintEventSource = new SqsEventSource(mintQueue, {batchSize: 1});
    metadataLambda.addEventSource(mintEventSource);

    const metadataJsonLambda = new Function(this, 'MetadataJson', {
      functionName: 'MetadataJson',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.metadataJsonHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 512,
      timeout: Duration.minutes(5),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 100
    })

    const metadataQueue = new sqs.Queue(this, 'metadata', {visibilityTimeout: Duration.minutes(5), queueName: 'metadata' });
    const metadataEventSource = new SqsEventSource(metadataQueue);
    metadataJsonLambda.addEventSource(metadataEventSource);

    const traitLambda = new Function(this, 'Trait', {
      functionName: 'Trait',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.traitHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 1024,
      timeout: Duration.minutes(15),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 20
    })

    const traitQueue = new sqs.Queue(this, 'traits', {visibilityTimeout: Duration.minutes(15), queueName: 'traits' });
    const traitEventSource = new SqsEventSource(traitQueue, {batchSize: 1});
    traitLambda.addEventSource(traitEventSource);

    const rarityLambda = new Function(this, 'Rarity', {
      functionName: 'Rarity',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.rarityHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 1024,
      timeout: Duration.minutes(15),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 20
    })

    const rarityQueue = new sqs.Queue(this, 'rarity', {visibilityTimeout: Duration.minutes(15), queueName: 'rarity' });
    const rarityEventSource = new SqsEventSource(rarityQueue, {batchSize: 1});
    rarityLambda.addEventSource(rarityEventSource);

    const rankLambda = new Function(this, 'Rank', {
      functionName: 'Rank',
      description: '',
      runtime: Runtime.NODEJS_14_X,
      handler: 'lambda.rankHandler',
      code: Code.fromAsset('../dist/lambda.zip'),
      memorySize: 1024,
      timeout: Duration.minutes(15),
      logRetention: RetentionDays.ONE_DAY,
      environment: {
        NODE_OPTIONS: '--enable-source-maps',
      },
      reservedConcurrentExecutions: 10
    })

    const rankQueue = new sqs.Queue(this, 'rank', {visibilityTimeout: Duration.minutes(15), queueName: 'rank' });
    const rankEventSource = new SqsEventSource(rankQueue, {batchSize: 1});
    rankLambda.addEventSource(rankEventSource);

    const discordQueue = new sqs.Queue(this, 'discord', {visibilityTimeout: Duration.minutes(15), queueName: 'discord' });

    const lambdas: Function[] = [
      websocketLambda,
      stateLambda,
      transactionLambda,
      metadataLambda,
      metadataJsonLambda,
      traitLambda,
      rarityLambda,
      rankLambda
    ]

    const sqsPolicy = new iam.PolicyStatement({
      actions: ['sqs:*'],
      effect: Effect.ALLOW,
      resources: ["*"],
    });

    const esPolicy = new iam.PolicyStatement({
      actions: ['es:*'],
      effect: Effect.ALLOW,
      resources: [
        "arn:aws:es:us-east-1:REDACTED:domain/REDACTED",
        "arn:aws:es:us-east-1:REDACTED:domain/REDACTED/*"
      ],
    });

    const dynamoPolicy = new iam.PolicyStatement({
      actions: ['dynamodb:*'],
      effect: Effect.ALLOW,
      resources: ["*"],
    });

    for (const lambda of lambdas){
      lambda.role?.attachInlinePolicy(
        new iam.Policy(this, `${lambda.node.id.toLowerCase()}-sqs-policy`, {
          statements: [sqsPolicy],
        }),
      );

      lambda.role?.attachInlinePolicy(
        new iam.Policy(this, `${lambda.node.id.toLowerCase()}-es-policy`, {
          statements: [esPolicy],
        }),
      );

      lambda.role?.attachInlinePolicy(
        new iam.Policy(this, `${lambda.node.id.toLowerCase()}-dynamo-policy`, {
          statements: [dynamoPolicy],
        }),
      );
    }

    const transactionsTable = new dynamodb.Table(this, 'Transactions', {
      tableName: 'Transactions',
      partitionKey: { name: 'id', type: dynamodb.AttributeType.STRING },
      // sortKey: {
      //   name: 'created_at',
      //   type: dynamodb.AttributeType.NUMBER
      // },
      billingMode: dynamodb.BillingMode.PROVISIONED,
      readCapacity: 25,
      writeCapacity: 10,
    });

    const mintsTable = new dynamodb.Table(this, 'Mints', {
      tableName: 'Mints',
      partitionKey: { name: 'id', type: dynamodb.AttributeType.STRING },
      billingMode: dynamodb.BillingMode.PROVISIONED,
      readCapacity: 100,
      writeCapacity: 100,
    });

    const collectionsTable = new dynamodb.Table(this, 'Collections', {
      tableName: 'Collections',
      partitionKey: { name: 'id', type: dynamodb.AttributeType.STRING },
      billingMode: dynamodb.BillingMode.PROVISIONED,
      readCapacity: 100,
      writeCapacity: 100,
    });

  }
}
